# HEAD


# v1.2.1

- add `fontSize` and `version` fields to `fontatlas` format


# v1.2.0

- remove polyline utils
- add svgpath reader for parsing SVG Path `d` attribute string
- add `util.geometry.path2d` package with `Path2D` class
- change `Path2D` to generate `Vec2`'s
- add `Triangulate` class for triangulating simple polygons


# v1.1.0

- add `LineStroker` class for rendering 2d lines
- refactor `Bezier3` to use `VertexConsumer` type and generate points in `LineStroker` manner
- refactor `Bezier4` to use `VertexConsumer` type and generate points in `LineStroker` manner
- add `SimpleVertexConsumer` class
- add `Lzw` class stub
- add MME `.fontatlas` format reader/writer


# v1.0.0

- Initial release
