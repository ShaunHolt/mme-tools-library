package mme.format.stl;

import mme.format.stl.Data;


class Reader {

	var i : haxe.io.Input;

	public function new(i) {
		this.i = i;
	}

/*
    UINT8[80] – Header
    UINT32 – Number of triangles


    foreach triangle
    REAL32[3] – Normal vector
    REAL32[3] – Vertex 1
    REAL32[3] – Vertex 2
    REAL32[3] – Vertex 3
    UINT16 – Attribute byte count
    end
*/

	public function read() : Data {

        i.bigEndian = false;

        var header = readHeader( i );

        if( header.getString( 0, 5 ) == "solid" ) {
            throw "ASCII STL format is not supported.";
        }

        var faces : Array<Face> = [];

        var numberOfFaces = i.readInt32();

        for( idx in 0...numberOfFaces ) {

            faces.push( readFace( i ) );
        }

        return {
            header: header,
            faces: faces,
        }
    }

    function readHeader( i : haxe.io.Input ) : haxe.io.Bytes {

        return i.read( 80 );
    }

    function readPoint3D( i : haxe.io.Input ) : Point3D {

        var x = i.readFloat();
        var y = i.readFloat();
        var z = i.readFloat();

        return {
            x: x,
            y: y,
            z: z,
        }
    }

    function readFace( i : haxe.io.Input ) : Face {

        var normal = readPoint3D( i );
        var p0 = readPoint3D( i );
        var p1 = readPoint3D( i );
        var p2 = readPoint3D( i );
        var attr = i.readInt16();

        return {
            normal: normal,
            p0: p0,
            p1: p1,
            p2: p2,
            attribute: attr,
        };
    }
}
