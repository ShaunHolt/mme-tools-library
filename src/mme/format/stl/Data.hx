package mme.format.stl;

typedef Point3D = {
    var x : Float;
    var y : Float;
    var z : Float;
}

typedef Face = {
    var normal : Point3D;
    var p0 : Point3D;
    var p1 : Point3D;
    var p2 : Point3D;
    var attribute : Int;
}

typedef Data = {
    var header : haxe.io.Bytes;
    var faces : Array<Face>;
}
