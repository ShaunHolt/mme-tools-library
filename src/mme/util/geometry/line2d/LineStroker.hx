package mme.util.geometry.line2d;

import mme.math.glmatrix.Vec2;
import mme.math.glmatrix.Vec3;

import mme.util.geometry.AggMath;


//-------------------------------------------------------------
//
// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_math_stroke.h
//
//-------------------------------------------------------------

//-----------------------------------------------------------------------
enum LineCapEnum
{
    butt_cap;
    square_cap;
    round_cap;
}

//-----------------------------------------------------------------------
enum LineJoinEnum
{
    miter_join;
    miter_join_revert;
    round_join;
    bevel_join;
    miter_join_round;
}


//-----------------------------------------------------------------------
enum InnerJoinEnum
{
    inner_bevel;
    inner_miter;
    inner_jag;
    inner_round;
}

//-----------------------------------------------------------------------
class LineStroker
{

    public function set_line_cap( lc:LineCapEnum)     { m_line_cap = lc; }
    public function set_line_join( lj:LineJoinEnum)   { m_line_join = lj; }
    public function set_inner_join( ij:InnerJoinEnum) { m_inner_join = ij; }

    public function get_line_cap()    { return m_line_cap; }
    public function get_line_join()   { return m_line_join; }
    public function get_inner_join()  { return m_inner_join; }

    public function set_miter_limit(ml : Float) { m_miter_limit = ml; }
    public function set_inner_miter_limit(ml : Float) { m_inner_miter_limit = ml; }
    public function set_approximation_scale(as : Float) { m_approx_scale = as; }

    public function get_miter_limit()  { return m_miter_limit; }
    public function get_inner_miter_limit()  { return m_inner_miter_limit; }
    public function get_approximation_scale()  { return m_approx_scale; }

    //-----------------------------------------------------------------------
    public function set_miter_limit_theta( t : Float )
    { 
        m_miter_limit = 1.0 / Math.sin(t * 0.5) ;
    }

    //-----------------------------------------------------------------------
    public function get_width()  { return m_width * 2.0; }
    public function set_width( w : Float )
    {
        m_width = w * 0.5; 
        if(m_width < 0)
        {
            m_width_abs  = -m_width;
            m_width_sign = -1;
        }
        else
        {
            m_width_abs  = m_width;
            m_width_sign = 1;
        }
        m_width_eps = m_width / 1024.0;
    }

    //-----------------------------------------------------------------------
    var m_width : Float;
    var m_width_abs : Float;
    var m_width_eps : Float;
    var m_width_sign : Int;
    var m_miter_limit : Float;
    var m_inner_miter_limit : Float;
    var m_approx_scale : Float;
    var m_line_cap : LineCapEnum;
    var m_line_join : LineJoinEnum;
    var m_inner_join : InnerJoinEnum;

    //-----------------------------------------------------------------------
    public function new() {
        m_width = 0.5;
        m_width_abs = 0.5;
        m_width_eps = 0.5/1024.0;
        m_width_sign = 1;
        m_miter_limit = 4.0;
        m_inner_miter_limit = 1.01;
        m_approx_scale = 1.0;
        m_line_cap = butt_cap;
        m_line_join = miter_join;
        m_inner_join = inner_miter;
    }

    //-----------------------------------------------------------------------
    public function calc_cap_quick(vc : VertexConsumer,
                             v0 : Vec3, 
                             v1 : Vec3, 
                            len : Float)
    {
        vc.remove_all();

        var dx1 : Float = (v1.y - v0.y) / len * m_width;
        var dy1 : Float = (v1.x - v0.x) / len * m_width;

        add_vertex(vc, v0.x - dx1, v0.y + dy1);
        add_vertex(vc, v0.x + dx1, v0.y - dy1);
    }
    //-----------------------------------------------------------------------
    public function calc_cap(vc : VertexConsumer,
                             v0 : Vec3, 
                             v1 : Vec3, 
                            len : Float)
    {
        vc.remove_all();

        var dx1 : Float = (v1.y - v0.y) / len;
        var dy1 : Float = (v1.x - v0.x) / len;
        var dx2 : Float = 0;
        var dy2 : Float = 0;

        dx1 *= m_width;
        dy1 *= m_width;

        if(m_line_cap != round_cap)
        {
            // add_vertex(vc, v0.x - dx1, v0.y + dy1);

            // if(m_line_cap == square_cap)
            // {
            //     var dx2 = dy1 * m_width_sign;
            //     var dy2 = dx1 * m_width_sign;
            //     add_vertex(vc, v0.x - dx1 - dx2, v0.y + dy1 - dy2);
            //     add_vertex(vc, v0.x + dx1 - dx2, v0.y - dy1 - dy2);
            // }

            // add_vertex(vc, v0.x + dx1, v0.y - dy1);

            if(m_line_cap == square_cap)
            {
                dx2 = dy1 * m_width_sign;
                dy2 = dx1 * m_width_sign;
            }
            add_vertex(vc, v0.x - dx1 - dx2, v0.y + dy1 - dy2);
            add_vertex(vc, v0.x + dx1 - dx2, v0.y - dy1 - dy2);
        }
        else
        {
            var da : Float = Math.acos(m_width_abs / (m_width_abs + 0.125 / m_approx_scale)) * 2;
            var a1 : Float;
            var i : Int;
            var n : Int = Std.int(Math.PI / da);

            da = Math.PI / (n + 1);
            add_vertex(vc, v0.x - dx1, v0.y + dy1);
            if(m_width_sign > 0)
            {
                a1 = Math.atan2(dy1, -dx1);
                a1 += da;
                for( i in 0...n )
                {
                    add_vertex(vc, v0.x + Math.cos(a1) * m_width, 
                                    v0.y + Math.sin(a1) * m_width);
                    a1 += da;
                }
            }
            else
            {
                a1 = Math.atan2(-dy1, dx1);
                a1 -= da;
                for( i in 0...n )
                {
                    add_vertex(vc, v0.x + Math.cos(a1) * m_width, 
                                    v0.y + Math.sin(a1) * m_width);
                    a1 -= da;
                }
            }
            add_vertex(vc, v0.x + dx1, v0.y - dy1);
        }
    }

    //-----------------------------------------------------------------------
    public function calc_join(vc : VertexConsumer,
                             v0 : Vec3, 
                             v1 : Vec3, 
                             v2 : Vec3,
                            len1 : Float, 
                            len2 : Float)
    {
        var dx1 : Float = m_width * (v1.y - v0.y) / len1;
        var dy1 : Float = m_width * (v1.x - v0.x) / len1;
        var dx2 : Float = m_width * (v2.y - v1.y) / len2;
        var dy2 : Float = m_width * (v2.x - v1.x) / len2;

        vc.remove_all();

        var cp : Float = cross_product(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y);
        if(cp != 0 && (cp > 0) == (m_width > 0))
        {
            // Inner join
            //---------------
            var limit : Float = ((len1 < len2) ? len1 : len2) / m_width_abs;
            if(limit < m_inner_miter_limit)
            {
                limit = m_inner_miter_limit;
            }

            switch(m_inner_join)
            {
            default: // inner_bevel
                add_vertex(vc, v1.x + dx1, v1.y - dy1);
                add_vertex(vc, v1.x + dx2, v1.y - dy2);

            case inner_miter:
                calc_miter(vc,
                            v0, v1, v2, dx1, dy1, dx2, dy2, 
                            miter_join_revert, 
                            limit, 0);

            // case inner_jag:
            // case inner_round:
            case inner_jag, inner_round:
                cp = (dx1-dx2) * (dx1-dx2) + (dy1-dy2) * (dy1-dy2);
                if(cp < len1 * len1 && cp < len2 * len2)
                {
                    calc_miter(vc,
                                v0, v1, v2, dx1, dy1, dx2, dy2, 
                                miter_join_revert, 
                                limit, 0);
                }
                else
                {
                    if(m_inner_join == inner_jag)
                    {
                        add_vertex(vc, v1.x + dx1, v1.y - dy1);
                        add_vertex(vc, v1.x,       v1.y      );
                        add_vertex(vc, v1.x + dx2, v1.y - dy2);
                    }
                    else
                    {
                        add_vertex(vc, v1.x + dx1, v1.y - dy1);
                        add_vertex(vc, v1.x,       v1.y      );
                        calc_arc(vc, v1.x, v1.y, dx2, -dy2, dx1, -dy1);
                        add_vertex(vc, v1.x,       v1.y      );
                        add_vertex(vc, v1.x + dx2, v1.y - dy2);
                    }
                }
            }
        }
        else
        {
            // Outer join
            //---------------

            // Calculate the distance between v1 and 
            // the central point of the bevel line segment
            //---------------
            var dxy : Vec2 = [ (dx1 + dx2) / 2, (dy1 + dy2) / 2 ];
            var dbevel : Float = Math.sqrt(dxy.x * dxy.x + dxy.y * dxy.y);

            if(m_line_join == round_join || m_line_join == bevel_join)
            {
                // This is an optimization that reduces the number of points 
                // in cases of almost collinear segments. If there's no
                // visible difference between bevel and miter joins we'd rather
                // use miter join because it adds only one point instead of two. 
                //
                // Here we calculate the middle point between the bevel points 
                // and then, the distance between v1 and this middle point. 
                // At outer joins this distance always less than stroke width, 
                // because it's actually the height of an isosceles triangle of
                // v1 and its two bevel points. If the difference between this
                // width and this value is small (no visible bevel) we can 
                // add just one point. 
                //
                // The constant in the expression makes the result approximately 
                // the same as in round joins and caps. You can safely comment 
                // out this entire "if".
                //-------------------
                if(m_approx_scale * (m_width_abs - dbevel) < m_width_eps)
                {
                    if(AggMath.calc_intersection(v0.x + dx1, v0.y - dy1,
                                            v1.x + dx1, v1.y - dy1,
                                            v1.x + dx2, v1.y - dy2,
                                            v2.x + dx2, v2.y - dy2,
                                            dxy))
                    {
                        add_vertex(vc, dxy.x, dxy.y);
                    }
                    else
                    {
                        add_vertex(vc, v1.x + dx1, v1.y - dy1);
                    }
                    return;
                }
            }

            switch(m_line_join)
            {
            // case miter_join:
            // case miter_join_revert:
            // case miter_join_round:
            case miter_join, miter_join_revert, miter_join_round:
                calc_miter(vc, 
                            v0, v1, v2, dx1, dy1, dx2, dy2, 
                            m_line_join, 
                            m_miter_limit,
                            dbevel);

            case round_join:
                calc_arc(vc, v1.x, v1.y, dx1, -dy1, dx2, -dy2);

            default: // Bevel join
                add_vertex(vc, v1.x + dx1, v1.y - dy1);
                add_vertex(vc, v1.x + dx2, v1.y - dy2);
            }
        }
    }

    //-----------------------------------------------------------------------
    // private
    //-----------------------------------------------------------------------
    inline function add_vertex(vc : VertexConsumer,  x:Float,  y:Float)
    {
        vc.add( x, y );
    }

    //-----------------------------------------------------------------------
    inline function cross_product(  x1 : Float, y1 : Float, 
                                    x2 : Float, y2 : Float, 
                                    x : Float,  y : Float) : Float
    {
        return (x - x2) * (y2 - y1) - (y - y2) * (x2 - x1);
    }

    //-----------------------------------------------------------------------
    function calc_arc(vc : VertexConsumer,
                    x : Float,   y : Float, 
                    dx1 : Float, dy1 : Float, 
                    dx2 : Float, dy2 : Float )
    {
        var a1 : Float = Math.atan2(dy1 * m_width_sign, dx1 * m_width_sign);
        var a2 : Float = Math.atan2(dy2 * m_width_sign, dx2 * m_width_sign);
        var da : Float = a1 - a2;
        var i : Int, n : Int;

        da = Math.Math.acos(m_width_abs / (m_width_abs + 0.125 / m_approx_scale)) * 2;

        add_vertex(vc, x + dx1, y + dy1);
        if(m_width_sign > 0)
        {
            if(a1 > a2) a2 += 2 * Math.PI;
            n = Std.int((a2 - a1) / da);
            da = (a2 - a1) / (n + 1);
            a1 += da;
            for( i in 0...n )
            {
                add_vertex(vc, x + Math.cos(a1) * m_width, y + Math.sin(a1) * m_width);
                a1 += da;
            }
        }
        else
        {
            if(a1 < a2) a2 -= 2 * Math.PI;
            n = Std.int((a1 - a2) / da);
            da = (a1 - a2) / (n + 1);
            a1 -= da;
            for( i in 0...n )
            {
                add_vertex(vc, x + Math.cos(a1) * m_width, y + Math.sin(a1) * m_width);
                a1 -= da;
            }
        }
        add_vertex(vc, x + dx2, y + dy2);
    }

    //-----------------------------------------------------------------------
    function calc_miter(vc : VertexConsumer,
                         v0 : Vec3, 
                         v1 : Vec3, 
                         v2 : Vec3,
                        dx1 : Float, dy1 : Float, 
                        dx2 : Float, dy2 : Float,
                        lj : LineJoinEnum,
                        mlimit : Float,
                        dbevel : Float)
    {
        var vi : Vec2 = [ v1.x, v1.y ];
        var di : Float  = 1;
        var lim : Float = m_width_abs * mlimit;
        var miter_limit_exceeded = true; // Assume the worst
        var intersection_failed  = true; // Assume the worst

        if(AggMath.calc_intersection(v0.x + dx1, v0.y - dy1,
                                v1.x + dx1, v1.y - dy1,
                                v1.x + dx2, v1.y - dy2,
                                v2.x + dx2, v2.y - dy2,
                                vi))
        {
            // Calculation of the intersection succeeded
            //---------------------
            di = AggMath.calc_distance( v1.x, v1.y, vi.x, vi.y );
            if(di <= lim)
            {
                // Inside the miter limit
                //---------------------
                add_vertex(vc, vi.x, vi.y);
                miter_limit_exceeded = false;
            }
            intersection_failed = false;
        }
        else
        {
            // Calculation of the intersection failed, most probably
            // the three points lie one straight line. 
            // First check if v0 and v2 lie on the opposite sides of vector: 
            // (v1.x, v1.y) -> (v1.x+dx1, v1.y-dy1), that is, the perpendicular
            // to the line determined by vertices v0 and v1.
            // This condition determines whether the next line segments continues
            // the previous one or goes back.
            //----------------
            var x2 : Float = v1.x + dx1;
            var y2 : Float = v1.y - dy1;
            if((cross_product(v0.x, v0.y, v1.x, v1.y, x2, y2) < 0.0) == 
                (cross_product(v1.x, v1.y, v2.x, v2.y, x2, y2) < 0.0))
            {
                // This case means that the next segment continues 
                // the previous one (straight line)
                //-----------------
                add_vertex(vc, v1.x + dx1, v1.y - dy1);
                miter_limit_exceeded = false;
            }
        }

        if(miter_limit_exceeded)
        {
            // Miter limit exceeded
            //------------------------
            switch(lj)
            {
            case miter_join_revert:
                // For the compatibility with SVG, PDF, etc, 
                // we use a simple bevel join instead of
                // "smart" bevel
                //-------------------
                add_vertex(vc, v1.x + dx1, v1.y - dy1);
                add_vertex(vc, v1.x + dx2, v1.y - dy2);

            case miter_join_round:
                calc_arc(vc, v1.x, v1.y, dx1, -dy1, dx2, -dy2);


            default:
                // If no miter-revert, calculate new dx1, dy1, dx2, dy2
                //----------------
                if(intersection_failed)
                {
                    mlimit *= m_width_sign;
                    add_vertex(vc, v1.x + dx1 + dy1 * mlimit, 
                                    v1.y - dy1 + dx1 * mlimit);
                    add_vertex(vc, v1.x + dx2 - dy2 * mlimit, 
                                    v1.y - dy2 - dx2 * mlimit);
                }
                else
                {
                    var x1 : Float = v1.x + dx1;
                    var y1 : Float = v1.y - dy1;
                    var x2 : Float = v1.x + dx2;
                    var y2 : Float = v1.y - dy2;
                    var di = (lim - dbevel) / (di - dbevel);
                    add_vertex(vc, x1 + (vi.x - x1) * di, 
                                    y1 + (vi.y - y1) * di);
                    add_vertex(vc, x2 + (vi.x - x2) * di, 
                                    y2 + (vi.y - y2) * di);
                }
            }
        }
    }
}
