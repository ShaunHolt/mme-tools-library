package mme.util.geometry.path2d;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;
import mme.math.glmatrix.Mat2d;
using mme.math.glmatrix.Mat2dTools;

import mme.util.geometry.path2d.Path2D;


class Arc2D {

    public var params ( get, null ) : Array<Float>;
        function get_params() return _params;
    public var center ( get, null ) : Vec2;
        function get_center() return _pc;
    public var rx ( get, null ) : Float;
        function get_rx() return _rx;
    public var ry ( get, null ) : Float;
        function get_ry() return _ry;
    public var start ( get, null ) : Angle;
        function get_start() return _startAngle;
    public var delta ( get, null ) : Angle;
        function get_delta() return _deltaAngle;
    public var xAxisRotation ( get, null ) : Angle;
        function get_xAxisRotation() return _xAxisRotation;
    public var matrix ( get, null ) : Mat2d;
        function get_matrix() return _transformationMatrix;

    var _params : Array<Float>;
    var _pc : Vec2;
    var _rx : Float;
    var _ry : Float;
    var _startAngle : Angle;
    var _deltaAngle : Angle;
    var _xAxisRotation : Angle;

    var _transformationMatrix : Mat2d;

    /**
    * Create a new Arc2D with the given values
    *
    * @param cx arc ellipse center X-coordinate
    * @param cy arc ellipse center Y-coordinate
    * @param rx arc ellipse X-radius
    * @param ry arc ellipse Y-radius
    * @param startAngle arc start angle, in degrees
    * @param deltaAngle arc sweep angle, in degrees
    * @param xAxisRotation arc ellipse X-axis rotation, in degrees
    */
    public function new( cx : Float, cy : Float, rx : Float, ry : Float, startAngle : Float, deltaAngle : Float, xAxisRotation : Float ) {

        if( Math.abs( deltaAngle ) < 0.0001 )
        {
            // TODO: degenerate to a Point() instead?
            throw "arc is too small!";
        }

        _params = [ cx, cy, rx, ry, startAngle, deltaAngle, xAxisRotation ];

        _pc = [ cx, cy ];
        _rx = rx;
        _ry = ry;
        _startAngle = new Angle( startAngle );
        _deltaAngle = new Angle( deltaAngle );
        _xAxisRotation = new Angle( xAxisRotation );

        _transformationMatrix = new Mat2d();
        _transformationMatrix = _transformationMatrix.translate([ cx, cy ]);
        _transformationMatrix = _transformationMatrix.rotate( _xAxisRotation.angleRad );
        _transformationMatrix = _transformationMatrix.scale([ _rx, _ry ]);
    }

/*
    Outline of SVG Path arc conversion:

    given: x1, y1, rx, ry, x-axis-rotation, fA, fS, x2, y2
        - use createFromSVGParameters() to find cx, cy, startAngle and endAngle
        var arc = Arc.createFromSVGParameters( x1, y1, rx, ry, phi, fA, fS, x2, y2 );

    arc.convert():
    - arc has:
        - center point
        - both axes: rx, ry
        - start angle
        - end angle
        - x-axis angle

    - simplify calculation down to the circle:
        - center (0,0)
        - radius (1.0)
        - start angle
        - end angle

    - all other values are accumulated in tranformation matrix (in this order):
        - rotate by x-axis-angle
        - center point -> translate to Pc
        - rx,ry -> scaleX rx, scaleY ry

    - use approximateArc() on the simplified circle to get array of Bezier points
    - apply transformation matrix to all the points and return as result

*/
    public function convert( ?matrix : Mat2d ) : Array< PathCommand >
    {
        if( matrix == null ) {

            matrix = new Mat2d();
        }

        var pc : Vec2 = [ 0, 0 ];
        var r = 1.0;
        var pstart = this.start.toPoint( r );

        var arrayOfBeziers = Arc2D.approximateArc( pstart, this.delta.angle, pc );

        var ctm = this.matrix.mul( matrix );

        return arrayOfBeziers.map( function( val ) return transformPathCommand( val, ctm ) );
    }

    function transformPathCommand( cmd : PathCommand, ctm : Mat2d ) {

        var transformedCmd;

        // TODO there should only ever be CPathMoveTo and CPathCubicCurveTo commands supplied to this function
        switch( cmd ) {

            case CPathMoveTo(p):
                transformedCmd = CPathMoveTo( ctm.multiplyVec2( p ) );

            case CPathLineTo(p):
                transformedCmd = CPathLineTo( ctm.multiplyVec2( p ) );

            case CPathQuadraticCurveTo(p2, p3):
                p2 = ctm.multiplyVec2( p2 );
                p3 = ctm.multiplyVec2( p3 );
                transformedCmd = CPathQuadraticCurveTo(p2, p3);

            case CPathCubicCurveTo(p2, p3, p4):
                p2 = ctm.multiplyVec2( p2 );
                p3 = ctm.multiplyVec2( p3 );
                p4 = ctm.multiplyVec2( p4 );
                transformedCmd = CPathCubicCurveTo(p2, p3, p4);

            // case CPathEllipticArcTo(p1, rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2):
                // p1 = ctm.multiplyVec2( p1 );
                // p2 = ctm.multiplyVec2( p2 );
                // rx, ry ???
                // transformedCmd = CPathEllipticArcTo(p1, rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2);
                // transformedCmd = cmd;

            default:
                transformedCmd = cmd;
        }

        return transformedCmd;
        
    }

    /**
    * Used when parsing SVG Path 'd' attribute. Implementation based on notes from
    * https://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter
    * parameters: last point x1, y1, Path Arc parameters: rx ry x-axis-rotation large-arc-flag sweep-flag x y
    */
    public static function createFromSVGParameters( x1 : Float, y1 : Float, rx : Float, ry : Float, phi : Float, fA : Int, fS : Int, x2 : Float, y2 : Float ) : Arc2D
    {
        var P1 : Vec2 = [ x1, y1 ];
        var P2 : Vec2 = [ x2, y2 ];

        // START - parameter checking
        // https://www.w3.org/TR/SVG/implnote.html#ArcOutOfRangeParameters
        if( P1.sub( P2 ).length() < 0.0001 )
        {
            throw "arc points too close";
        }
        if( GLMatrix.equals( rx, 0.0 ) || GLMatrix.equals( ry, 0.0 ) )
        {
            throw "radius == 0";
        }
        rx = Math.abs( rx );
        ry = Math.abs( ry );
        phi %= 360.0;

        var phiDeg = phi;
        phi = GLMatrix.toRadian( phi );
        // END - parameter checking

        // Step 1
        // Un-rotate the mid-point between P1 and P2
        var P1p = P1.sub( P2 ).scale( 0.5 ).rotate( -phi );

        // https://www.w3.org/TR/SVG/implnote.html#ArcCorrectionOutOfRangeRadii
        // Make sure we have at least one solution
        var lambda = ( P1p.x * P1p.x ) / ( rx * rx ) + ( P1p.y * P1p.y ) / ( ry * ry );
        if( lambda > 1.0 )
        {
            lambda = Math.sqrt( lambda );
            rx *= lambda;
            ry *= lambda;
        }

        // Step 2
        // Find ellipse center point
        var cxp = rx * P1p.y / ry;
        var cyp = - ry * P1p.x / rx;
        var coef = Math.sqrt( ( rx*rx*ry*ry - rx*rx*P1p.y*P1p.y - ry*ry*P1p.x*P1p.x ) / ( rx*rx * P1p.y*P1p.y + ry*ry * P1p.x*P1p.x ) );
        if( fA == fS )
        {
            coef = - coef;
        }
        cxp *= coef;
        cyp *= coef;

        // Step 3
        // rotate back the center to it's correct position (to undo what we did in Step 1)
        var Pcp = Vec2.fromValues( cxp, cyp ).rotate( phi );
        var Pc = Pcp.rotate( phi ).add( P1.add( P2 ).scale( 0.5 ) );

        // Step 4
        var angleFromTo = function( p1 : Vec2, p2 : Vec2 ) {
            var angle = Math.atan2( p2.y, p2.x ) - Math.atan2( p1.y, p1.x );
            return angle / GLMatrix.degree;
        }
        // Find starting angle (for P1) and arc angle (the angle between P1 and P2)
        // by using points in 'un-rotated' space (P1p and cxp, cyp)
        var unitX : Vec2 = [ 1, 0 ];
        var first : Vec2 = [ ( P1p.x - cxp ) / rx, ( P1p.y - cyp ) / ry ];
        // var theta1 = unitX.angle( first ) / GLMatrix.degree;
        var theta1 = angleFromTo( unitX, first );

        var second : Vec2 = [ ( - P1p.x - cxp ) / rx, ( - P1p.y - cyp ) / ry ];
        // var thetaD = first.angle( second ) / GLMatrix.degree;
        var thetaD = angleFromTo( first, second );

        // Sys.println('thetaD" $thetaD');

        if( fS == 0 && thetaD > 0 )
        {
            thetaD -= 360;
        }
        if( fS == 1 && thetaD < 0 )
        {
            thetaD += 360;
        }

        // Sys.println('first $first');
        // Sys.println('second $second');
        // Sys.println('theta1 $theta1');
        // Sys.println('thetaD $thetaD');

        return new Arc2D( Pc.x, Pc.y, rx, ry, theta1, thetaD, phiDeg );
    }

    /**
     Approximates circular arc with center in Pc and start point pstart.
     Arc spans deltaAngle degrees: clockwise if deltaAngle > 0,
     and counterclockwise if deltaAngle < 0.

     @param pstart circular arc start point
     @param deltaAngle arc angle in degrees
     @param Pc center of circle the arc belongs to
     @return array of `CPathCubicCurveTo` commands
     **/
    public static function approximateArc( pstart : Vec2, deltaAngle : Float, Pc : Vec2 ) : Array<PathCommand>
    {
        // We need to have angles in range 0 < angle < +/-360 in order
        // to properly calculate number of 90-deg arc segments
        deltaAngle %= 360.0;

        var absAngle = Math.abs( deltaAngle );
        if( absAngle < 0.0001 )
        {
            // TODO: degenerate to a Point() instead ?
            throw "Arc is too small!";
        }
        var angleSign = deltaAngle / absAngle;


        var translatePoints = function( points : Array<Vec2>, targetPoint : Vec2 )
        {
            var newPoints = [];
            for( idx in 0...points.length )
            {
                newPoints.push( points[ idx ].add( targetPoint ) );
            }

            return newPoints;
        }

        // translate to (0,0)
        pstart = pstart.sub( Pc );

        var numberOfQuarters = Math.floor( absAngle / 90 );
        var restOfTheArc = absAngle - numberOfQuarters * 90;

        var result = [];
        var currentPoint = pstart;
        var angle90 = angleSign * 90;
        restOfTheArc = angleSign * restOfTheArc;

        result.push( CPathMoveTo( currentPoint ) );

        for( n in 0...numberOfQuarters )
        {
            // trace('currentPoint ${currentPoint.x}, ${currentPoint.y}');
            var points = Arc2D.approximateArcBasic( currentPoint, angle90 );
            currentPoint = points[3].clone();

            points = translatePoints( points, Pc );
            result.push( CPathCubicCurveTo( 
                points[1],
                points[2], points[3]
                ) );
        }

        // trace('currentPoint ${currentPoint.x}, ${currentPoint.y}');
        // trace('restOfTheArc $restOfTheArc');

        if( ! GLMatrix.equals( restOfTheArc, 0.0 ) ) {

            var points = Arc2D.approximateArcBasic( currentPoint, restOfTheArc );
            points = translatePoints( points, Pc );
            result.push( CPathCubicCurveTo( 
                    points[1],
                    points[2], points[3]
                ) );
        }

        return result;
    }

    /**
    Calculates 4 points of cubic Bezier curve that approximates circular arc
    defined with given parameters.

    @param pstart circular arc start point, assumes arc center at (0,0)
    @param angle arc angle in degrees, should be in +/-90 range
    @return array of 4 (x,y) points, representing cubic Bezier control points
    **/
    public static function approximateArcBasic( pstart : Vec2, angle : Float ) : Array<Vec2>
    {
    /*
        Based on http://stackoverflow.com/a/40327968

        parameters: (pstart, angle)
            - assume (xc,yc) = (0,0)
            - then R = pstart.length()
            - -90 < angle < 90
            - arc starts at pstart

        we'll calculate bezier points for case when P0 is at (R,0)
        and then just rotate them by angle between pstart and X-axis

        - assume P0 = (R,0)
        - find P3 for (angle): (Rsin(angle),Rcos(angle))
        - calculate Bezier4 points
        - rotate Bezier4 points by pstart.angle(1,0)

    */
        angle %= 360;
        if( Math.abs( angle ) > 90 )
        {
            throw 'approximateArcBasic: abs( angle ) > 90 degrees';
        }

        angle = GLMatrix.toRadian( angle );
        var R = pstart.length();
        var P0 : Vec2 = [ R, 0 ];
        var P3 = P0.rotate( angle );

        // 0 < phi < PI/4
        // which means:
        // 0 < angle < 90
        var phi = angle / 2;

        // text on SO has error here
        // missing '/' between '3' and '('
        var k = ( 4 / 3 ) / ( 1 / Math.cos( phi ) + 1 );
        // var k = 0.55191502449

        // PM = (P0 + P3) / 2
        var PM = P0.add( P3 ).scale( 0.5 );

        // PH = PM / cos(x) = PM sec(x) = (P0 + P3) sec(x) / 2
        var PH = PM.scale( 1 / Math.cos( phi ) );

        // PE = PH / cos(x) = PM sec(x)^2 = (P0 + P3) sec(x)^2 / 2
        var PE = PH.scale( 1 / Math.cos( phi ) );

        // P1 = (1 - k)P0 + k PE
        // var P1 = P0.mul( 1 - k ).add( PE.mul( k ) )
        // or
        // P1 = P0 + k (PE - P0)
        var P1 = P0.add( PE.sub( P0 ).scale( k ) );

        // P2 = (1 - k)P3 + k PE
        // var P2 = P3.mul( 1 - k ).add( PE.mul( k ) )
        // or
        // P2 = P3 + k (PE - P3)
        var P2 = P3.add( PE.sub( P3 ).scale( k ) );

        var backAngle = Angle.fromPoint( pstart );
        P0 = P0.rotate( backAngle.angleRad );
        P1 = P1.rotate( backAngle.angleRad );
        P2 = P2.rotate( backAngle.angleRad );
        P3 = P3.rotate( backAngle.angleRad );

        return [ P0, P1, P2, P3 ];
    }
}
