package mme.util.geometry.path2d;

import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;


class Angle {

    public var angleRad ( default, null ) : Float;
    public var angle ( get, set ) : Float;
    public var sin ( default, null ) : Float;
    public var cos ( default, null ) : Float;

    var _angleDeg : Float;

    public function new( angle : Float ) {

        _angleDeg = 0.0;
        angleRad = 0.0;
        sin = 0.0;
        cos = 1.0;

        set_angle( angle );
    }

    function get_angle() : Float {
        return _angleDeg;
    }

    function set_angle( angle : Float ) : Float {

        if( ! Math.isFinite( angle ) ) return angle;

        // Constrain to values between -360 and +360
        // because we need to be able to express angle difference with this also
        // and the difference can be > +180 or < -180, depending on values of
        // lhs and rhs of subtraction operation
        if( Math.abs( angle ) == 360.0 ) {

            _angleDeg = angle;

        } else {

            _angleDeg = angle % 360;
        }

        angleRad = GLMatrix.toRadian( _angleDeg );

        // angleRad = utils.roundToPrecision( _angleRad );
        // angleDeg = utils.roundToPrecision( _angleDeg );
        sin = Math.sin( angleRad );
        cos = Math.cos( angleRad );

        return _angleDeg;

    }

    public function neg() {

        return fromPoint([ cos, -sin ]);
    }

    public function add( other : Angle ) : Angle {

        return new Angle( this.angle + other.angle );
    }

    public function sub( other : Angle ) : Angle {

        return new Angle( this.angle - other.angle );
    }

    /** Returns a point which is at 'length' distance from (0,0) and at an angle to X-axis equal to this angle */
    public function toPoint( length : Float ) : Vec2 {

        if( ! Math.isFinite( length ) ) {

            // This assumes length == 1.0
            return [ this.cos, this.sin ];
        }

        return [ this.cos * length, this.sin * length ];
    }

    // Angle can be defined via a 2D space point as an
    // andgle between vector [origin, Point] and X-axis
    public static function fromPoint( pt : Vec2 ) {

        var _angleRad = 0.0;

        var length = pt.len();

        if( GLMatrix.equals( length, 0.0 ) ) {

            _angleRad = 0.0;

        } else {

            _angleRad = Math.atan2( pt.y, pt.x );
        }

        return new Angle( _angleRad / GLMatrix.degree );
    }
}
