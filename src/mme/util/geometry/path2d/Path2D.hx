package mme.util.geometry.path2d;

import mme.format.svgpath.Data;
import mme.format.svgpath.Reader;

import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;

import mme.util.geometry.curve2d.Bezier3;
import mme.util.geometry.curve2d.Bezier4;
import mme.util.geometry.AggMath.SimpleVertexConsumer;


enum PathCommand {
    CPathMoveTo( p : Vec2 );
    CPathLineTo( p : Vec2 );
    CPathQuadraticCurveTo( p2 : Vec2, p3 : Vec2 );
    CPathCubicCurveTo( p2 : Vec2, p3 : Vec2, p4 : Vec2 );
    CPathEllipticArcTo( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, p2 : Vec2 );
    CPathClosePath;
}

class Path2D {

    var _d : String;
    public var approximationScale : Float;

    public var pathData ( default, null ) : Array<String>;
    public var pathCommands ( default, null ) : Array<PathCommand>;

    // https://www.w3.org/TR/SVG/paths.html#PathDataBNF
    var _pathDataRegex : EReg = ~/[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|\s*[MmZzLlHhVvCcSsQqTtAa]\s*/g;

    public function new( ?d : String, ?commands : Array<PathCommand> ) {

        _d = '';
        pathData = [];
        pathCommands = [];

        approximationScale = 0.125;

        if( d != null ) {

            _d = d;
            var reader = new Reader( _d );
            var data = reader.read();
            pathData = reader.pathData;

            pathCommandsFromElements( data.elements );

        } else if( commands != null ) {

            for( c in commands ) pathCommands.push( c );
        }
    }

    public function moveTo( p : Vec2 ) {
        pathCommands.push( CPathMoveTo( p ) );
    }

    public function lineTo( p : Vec2 ) {
        pathCommands.push( CPathLineTo( p ) );
    }

    public function quadraticCurveTo( p2 : Vec2, p3 : Vec2 ) {
        pathCommands.push( CPathQuadraticCurveTo( p2, p3 ) );
    }

    public function cubicCurveTo( p2 : Vec2, p3 : Vec2, p4 : Vec2 ) {
        pathCommands.push( CPathCubicCurveTo( p2, p3, p4 ) );
    }

    public function ellipticArcTo( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, p2 : Vec2 ) {
        pathCommands.push( CPathEllipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2) );
    }

    public function closePath() {
        pathCommands.push( CPathClosePath );
    }

    public function convert() : Array<Vec2> {

        if( pathCommands.length > 0 && ! pathCommands[0].match(CPathMoveTo(_)) ) {
            throw 'path commands must start with "moveTo" ( CPathMoveTo(), "M" or "m" ) command';
        }

        return convertCommandList( pathCommands, approximationScale );
    }

    public static function convertCommandList( commands : Array<PathCommand>, approximationScale : Float = 0.125 ) : Array<Vec2> {

        var pointStartSubPath : Vec2 = [ Math.NaN, 1.0 ];
        var pointCloseSubPath : Vec2 = [ Math.NaN, -1.0 ];
        var bezier3 = new Bezier3();
        bezier3.set_approximation_scale( approximationScale );
        var bezier4 = new Bezier4();
        bezier4.set_approximation_scale( approximationScale );

        var points : Array<Vec2> = [];
        var currentPoint : Vec2 = [ 0, 0 ];
        var subPathStartPoint = currentPoint;


        for( cmd in commands ) {

            switch( cmd ) {

                case CPathMoveTo(p):

                    points.push( pointStartSubPath );
                    points.push( [ p.x, p.y ] );

                    currentPoint = [ p.x, p.y ];

                    // Reset the subpath start point for CClosePath
                    subPathStartPoint = currentPoint;

                case CPathLineTo(p):

                    points.push( [ p.x, p.y ] );

                    currentPoint = [ p.x, p.y ];

                case CPathQuadraticCurveTo(p2, p3):

                    var vc = new SimpleVertexConsumer();
                    bezier3.bezier( vc, currentPoint, p2, p3 );

                    // skip first point since it is the currentPoint and
                    // has already been added by any of the previous commands
                    for( idx in 1...vc.vertices.length ) {
                        points.push( vc.vertices[ idx ] );
                    }

                    currentPoint = [ p3.x, p3.y ];

                case CPathCubicCurveTo(p2, p3, p4):

                    var vc = new SimpleVertexConsumer();
                    bezier4.bezier( vc, currentPoint, p2, p3, p4 );

                    // skip first point since it is the currentPoint and
                    // has already been added by any of the previous commands
                    for( idx in 1...vc.vertices.length ) {
                        points.push( vc.vertices[ idx ] );
                    }

                    currentPoint = [ p4.x, p4.y ];

                case CPathEllipticArcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2):

                    var arc = Arc2D.createFromSVGParameters( currentPoint.x, currentPoint.y, rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2.x, p2.y );
                    var arcCommands = arc.convert();

                    var arcPoints = convertCommandList( arcCommands, approximationScale );
                    // remove initial CPathMoveTo(currentPoint) command point since we don't need it in this context,
                    // that point has already been added by any of the previous commands
                    arcPoints.shift(); // remove [nan, 1, 0] <- pointStartSubPath
                    arcPoints.shift(); // remove moveto point
                    for( v in arcPoints ) {
                        points.push( v );
                    }

                    currentPoint = [ p2.x, p2.y ];

                case CPathClosePath:

                    points.push( pointCloseSubPath );

                    var x = subPathStartPoint.x;
                    var y = subPathStartPoint.y;

                    currentPoint = [ x, y ];

                default:
            }
        }

        return points;
    }

    function pathCommandsFromElements( elements : Array<PathElement> ) : Void {

        var currentPoint : Vec2 = [ 0, 0 ];
        var subPathStartPoint = currentPoint;
        var prevControlPoint = currentPoint;

        if( elements.length > 0 && ! elements[0].match(CMoveTo(_)) ) {
            throw 'path commands string must start with "moveTo" ( "M" or "m" ) command';
        }

        for( el in elements ) {

            switch( el ) {

                case CMoveTo(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    moveTo([ x, y ]);

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                    // Reset the subpath start point for CClosePath
                    subPathStartPoint = currentPoint;

                case CLineTo(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    lineTo([ x, y ]);

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                case CLineToHorizontal(x, relative):
                    var y = currentPoint.y;
                    if( relative )
                    {
                        x += currentPoint.x;
                    }

                    lineTo([ x, y ]);

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                case CLineToVertical(y, relative):
                    var x = currentPoint.x;
                    if( relative )
                    {
                        y += currentPoint.y;
                    }

                    lineTo([ x, y ]);

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                case CQuadraticBezier(x1, y1, x, y, relative):
                    if( relative )
                    {
                        x1 += currentPoint.x;
                        y1 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    quadraticCurveTo( [x1, y1], [x, y ]);

                    // Save control point for 'CQuadraticBezierSmooth' element
                    prevControlPoint = [ x1, y1 ];

                    currentPoint = [ x, y ];

                case CQuadraticBezierSmooth(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    if( prevControlPoint == null ) prevControlPoint = currentPoint;

                    // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                    // We need to rotate prevControlPoint 180 degrees around currentPoint
                    // to get the control point for this curve.
                    // Rotating point (x,y) 180 degrees around point (x0, y0):
                    // x' = 2 * x0 - x
                    // y' = 2 * y0 - y
                    var x1 = 2 * currentPoint.x - prevControlPoint.x;
                    var y1 = 2 * currentPoint.y - prevControlPoint.y;

                    quadraticCurveTo( [x1, y1], [x, y ]);

                    // Save control point for 'CQuadraticBezierSmooth' element
                    prevControlPoint = [ x1, y1 ];

                    currentPoint = [ x, y ];

                case CCubicBezier(x1, y1, x2, y2, x, y, relative):
                    if( relative )
                    {
                        x1 += currentPoint.x;
                        y1 += currentPoint.y;
                        x2 += currentPoint.x;
                        y2 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    cubicCurveTo( [x1, y1], [x2, y2], [x, y] );

                    // Save control point for 'CCubicBezierSmooth' element
                    prevControlPoint = [ x2, y2 ];

                    currentPoint = [ x, y ];

                case CCubicBezierSmooth(x2, y2, x, y, relative):
                    if( relative )
                    {
                        x2 += currentPoint.x;
                        y2 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    if( prevControlPoint == null ) prevControlPoint = currentPoint;

                    // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                    // We need to rotate prevControlPoint 180 degrees around currentPoint
                    // to get the control point for this curve.
                    // Rotating point (x,y) 180 degrees around point (x0, y0):
                    // x' = 2 * x0 - x
                    // y' = 2 * y0 - y
                    var x1 = 2 * currentPoint.x - prevControlPoint.x;
                    var y1 = 2 * currentPoint.y - prevControlPoint.y;

                    cubicCurveTo( [x1, y1], [x2, y2], [x, y] );

                    // Save control point for 'CCubicBezierSmooth' element
                    prevControlPoint = [ x2, y2 ];

                    currentPoint = [ x, y ];

                case CEllipticArc(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    ellipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, [x, y ]);

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                case CClosePath:

                    var x = subPathStartPoint.x;
                    var y = subPathStartPoint.y;

                    closePath();

                    prevControlPoint = null;
                    currentPoint = [ x, y ];

                default:
            }
        }
    }
}
