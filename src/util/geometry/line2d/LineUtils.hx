package mme.util.geometry.line2d;

import mme.math.glmatrix.Vec2;

using mme.math.glmatrix.Vec2Tools;
using Lambda;

class OutlineData {
	public var points : Array<Vec2>;
	public var normals : Array<Vec2>;
	public var miters : Array<Float>;
	public var indices : Array<Int>;

	public function new( polylinePoints, miterMormals, miterLengths, triangleIndices ) {
		points = polylinePoints;
		normals = miterMormals;
		miters = miterLengths;
		indices = triangleIndices;
	}
}

class LineUtils {

    public static function createOutline( path : Array<Vec2>, closed : Bool = false ) : OutlineData {

        var miterVectors = PolylineNormals.getNormals( path, closed );

        if( closed ) {
            path = path.copy();
            path.push(path[0]);
            miterVectors.push(miterVectors[0]);
        }

        var miterMormals = miterVectors.map(function(x) return x.norm);
        var miterLengths = miterVectors.map(function(x) return x.len);

        miterMormals = duplicateVectors( miterMormals );
        miterLengths = duplicateFloats( miterLengths, true );

        var polylinePoints = duplicateVectors( path );
        var triangleIndices = createIndices( path.length );

		var result = new OutlineData( polylinePoints, miterMormals, miterLengths, triangleIndices );

		return result;
    }

    /**
    Create indices to be used to construct triangles by picking points
    from our array of calculated miter vectors (created by PolylineNormals.getNormals()).
    Once we have an array of miter vectors, it will be populated by vectors for only
    one side of the polyline. The array of miter vectors will give us a string of
    points that comprise only one side of the polyline outline shape. To get the points
    for the other side we will simply mirror miter vectors by calling duplicateVectors() and
    duplicateFloats() functions.

    Original miter vectors, with their array indexes:
```
    0     1     3
    ^     ^     ^
    |     |     |
    +-----+-----+
```
    Miter vectors after mirroring, with their array indexes:
```
    0     2     4
    ^     ^     ^
    |     |     |
    +-----+-----+
    |     |     |
    v     v     v
    1     3     5
```
    In order to render generated outline for our polyline, we need triangles whose
    points are determined by adding miter vectors to polyline points:

```
    0   2   4
    ^---^---^
    |  /|  /|
    + / + / +
    |/  |/  |
    v---v---v
    1   3   5
```
    From the diagram above we can see that indexes for points of the triangles we need to
    render are, in counter-clockwise direction:
```
    T1: 0,1,2
    T2: 2,1,3
    T3: 2,3,4
    T4: 4,3,5
    etc.
```
    This function generates those indices.

    @param {Int} Number of elements the array of miter vectors
    @returns {Array<Int>} Array of index triplets defining points for
        CCW triangles to be used to render outline shape
     */
    public static function createIndices( length : Int ) : Array<Int> {

        var indices = new Array<Int>();
        var c = 0, index = 0;

        for ( j in 0...length-1 ) {

            var i = index;

            // T1
            indices[c++] = i + 0;
            indices[c++] = i + 1;
            indices[c++] = i + 2;
            // T2
            indices[c++] = i + 2;
            indices[c++] = i + 1;
            indices[c++] = i + 3;

            index += 2;
        }

        return indices;
    }

    // original code uses duplicate() function to duplicate two structurally
    // *different* arrays: one is an array of arrays the other is array of numbers
    // `mirror` parameter has two meanings:
    //      - one is to tell update() to negate duplicate value
    //      - the other is to signal update() if it is:
    //          - processing array of arrays (`mirror` == falsey)
    //          - processing array of numbers (`mirror` === true)
    // N.B. it took an hour to finally untangle this one crap

    //we need to duplicate vertex attributes to expand in the shader
    //and mirror the normals

    // version that works on 2D vectors
    public static function duplicateVectors( values : Array<Vec2> ) : Array<Vec2> {

        var out : Array<Vec2> = [];

        values.iter(function(x) {
            out.push(x.clone());
            out.push(x.clone());
        });

        return out;
    }

    // version that works on numbers
    // technically, the `mirror` parameter is not needed at all since this version
    // is *only* called with `mirror` == true
    public static function duplicateFloats( values : Array<Float>, mirror : Bool = false ) : Array<Float> {

        var out : Array<Float> = [];

        values.iter(function(x) {
            var x1 = mirror ? -x : x;
            out.push(x);
            out.push(x1);
        });

        return out;
    }
}
