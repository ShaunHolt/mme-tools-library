package mme.format.mtl;

import mme.format.mtl.Data;

using StringTools;

// http://paulbourke.net/dataformats/mtl/
class Reader {

    var i : haxe.io.Input;
    var currentInputLine : Int;

    public function new( i : haxe.io.Input ) {
        this.i = i;
        currentInputLine = 0;
    }

    public function read() : Data {

        // newmtl name
        var reg_newmtl : EReg = ~/^newmtl\s+([^\s]+)\s*$/i;
        // Ka r g b
        var reg_Ka : EReg = ~/^Ka\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/i;
        // Kd r g b
        var reg_Kd : EReg = ~/^Kd\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/i;
        // Ks r g b
        var reg_Ks : EReg = ~/^Ks\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/i;
        // Ke r g b
        var reg_Ke : EReg = ~/^Ke\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/i;
        // Ns value
        var reg_Ns : EReg = ~/^Ns\s+([^\s]+)\s*$/i;
        // d value
        var reg_d : EReg = ~/^d\s+([^\s]+)\s*$/i;
        // illum value
        var reg_illum : EReg = ~/^illum\s+(\d+)\s*$/i;


        var data : Data = [];

        try {

            // loop ends on haxe.io.Eof exception
            while( true ) {

                var line = i.readLine().trim();

                currentInputLine++;

                line = line.trim();

                if( line.length == 0 || line.startsWith('#') ) {
                    continue;
                }

                if( reg_newmtl.match( line ) ) {

                    data.push( parseNewMaterial( reg_newmtl ) );
                    continue;
                }

                if( reg_Ka.match( line ) ) {

                    data.push( parseKa( reg_Ka ) );
                    continue;
                }

                if( reg_Kd.match( line ) ) {

                    data.push( parseKd( reg_Kd ) );
                    continue;
                }

                if( reg_Ks.match( line ) ) {

                    data.push( parseKs( reg_Ks ) );
                    continue;
                }

                if( reg_Ke.match( line ) ) {

                    data.push( parseKe( reg_Ke ) );
                    continue;
                }

                if( reg_Ns.match( line ) ) {

                    data.push( parseNs( reg_Ns ) );
                    continue;
                }

                if( reg_d.match( line ) ) {

                    data.push( parseD( reg_d ) );
                    continue;
                }

                if( reg_illum.match( line ) ) {

                    data.push( parseIllumination( reg_illum ) );
                    continue;
                }

                data.push( CUnknown( line ) );
            }

        } catch( eof: haxe.io.Eof ) { }

        return data;
    }

    function parseKa( reg : EReg ) {

        var r = Std.parseFloat( reg.matched(1) );
        var g = Std.parseFloat( reg.matched(2) );
        var b = Std.parseFloat( reg.matched(3) );

        return CKa({
            r: r,
            g: g,
            b: b,
        });
    }

    function parseKd( reg : EReg ) {

        var r = Std.parseFloat( reg.matched(1) );
        var g = Std.parseFloat( reg.matched(2) );
        var b = Std.parseFloat( reg.matched(3) );

        return CKd({
            r: r,
            g: g,
            b: b,
        });
    }

    function parseKs( reg : EReg ) {

        var r = Std.parseFloat( reg.matched(1) );
        var g = Std.parseFloat( reg.matched(2) );
        var b = Std.parseFloat( reg.matched(3) );

        return CKs({
            r: r,
            g: g,
            b: b,
        });
    }

    function parseKe( reg : EReg ) {

        var r = Std.parseFloat( reg.matched(1) );
        var g = Std.parseFloat( reg.matched(2) );
        var b = Std.parseFloat( reg.matched(3) );

        return CKe({
            r: r,
            g: g,
            b: b,
        });
    }

    function parseNewMaterial( reg : EReg) {

        var materialName = reg.matched(1).trim();

        return CMaterial( materialName );
    }

    function parseNs( reg : EReg ) {

        var value = Std.parseFloat( reg.matched(1) );

        return CNs(value);
    }

    function parseD( reg : EReg ) {

        var value = Std.parseFloat( reg.matched(1) );

        return Cd(value);
    }

    function parseIllumination( reg : EReg ) {

        var value = Std.parseInt( reg.matched(1) );

        return CIllumination(value);
    }
}
