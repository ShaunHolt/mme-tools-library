package mme.format.obj;

typedef FaceVertex = {
    var vertexIndex : Int;
    var uvIndex : Null<Int>;
    var normalIndex : Null<Int>;
}

typedef Face = Array< FaceVertex >;

typedef VertexParametric = {

    var u : Float;
    var v : Float;
    var w : Float;
}

typedef VertexNormal = {

    var i : Float;
    var j : Float;
    var k : Float;
}

typedef VertexTexture = {

    var u : Float;
    var v : Float;
    var w : Float;
}

typedef Vertex = {

    var x : Float;
    var y : Float;
    var z : Float;
    var w : Float;
}

enum Line {

    CEnd;

    CVertex( v : Vertex );
    CNormal( vn : VertexNormal );
    CTextureUV( vt : VertexTexture );
    CFace( f : Face );
    CGroup( g : Array<String> );
    CObject( o : String );
    CParametric( vp : VertexParametric );
    CMtlLib( mtllib : Array<String> );
    CMaterial( usemtl : String );

    CUnknown( l : String );
}

typedef Data = Array< Line >;
