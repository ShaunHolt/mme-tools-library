package mme.format.obj;

import mme.format.obj.Data;

using StringTools;

// http://www.martinreddy.net/gfx/3d/OBJ.spec
// http://paulbourke.net/dataformats/obj/
class Reader {

    var i : haxe.io.Input;
    var currentInputLine : Int;

    public function new( i : haxe.io.Input ) {
        this.i = i;
        currentInputLine = 0;
    }

    public function read() : Data {

        // v x y z w?
        var reg_v : EReg = ~/^v\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)(?:\s+([^\s]+))?\s*$/i;
        // vt u v? w?
        var reg_vt : EReg = ~/^vt\s+([^\s]+)(?:\s+([^\s]+))?(?:\s+([^\s]+))?\s*$/i;
        // vn i j k
        var reg_vn : EReg = ~/^vn\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/i;
        // vp u v w?
        var reg_vp : EReg = ~/^vp\s+([^\s]+)\s+([^\s]+)(?:\s+([^\s]+))?\s*$/i;
        // g name*
        var reg_g : EReg = ~/^g((?:\s+[^\s]+)*)\s*$/i;
        // o name?
        var reg_o : EReg = ~/^o(?:\s+([^\s]+))?\s*$/i;
        // f 9/9?/9? 9/9?/9? 9/9?/9? (9/9?/9?)*
        var reg_f : EReg = ~/^f((?:\s+[^\s]+){3,})\s*$/i;
        // mtllib name+
        var reg_mtllib : EReg = ~/^mtllib((?:\s+[^\s]+)+)\s*$/i;
        // usemtl name
        var reg_usemtl : EReg = ~/^usemtl\s+([^\s]+)\s*$/i;


        var data : Data = [];

        try {

            // loop ends on haxe.io.Eof exception
            while( true ) {

                var line = i.readLine().trim();

                currentInputLine++;

                line = line.trim();

                if( line.length == 0 || line.startsWith('#') ) {
                    continue;
                }

                if( reg_v.match( line ) ) {

                    data.push( parseVertex( reg_v ) );
                    continue;
                }

                if( reg_vt.match( line ) ) {

                    data.push( parseVertexTexture( reg_vt ) );
                    continue;
                }

                if( reg_vn.match( line ) ) {

                    data.push( parseVertexNormal( reg_vn ) );
                    continue;
                }

                if( reg_f.match( line ) ) {

                    data.push( parseFace( reg_f ) );
                    continue;
                }

                if( reg_vp.match( line ) ) {

                    data.push( parseVertex( reg_vp ) );
                    continue;
                }

                if( reg_usemtl.match( line ) ) {

                    data.push( parseUseMaterial( reg_usemtl ) );
                    continue;
                }

                if( reg_g.match( line ) ) {

                    data.push( parseGroupName( reg_g ) );
                    continue;
                }

                if( reg_o.match( line ) ) {

                    data.push( parseObjectName( reg_o ) );
                    continue;
                }

                if( reg_mtllib.match( line ) ) {

                    data.push( parseMaterialLibrary( reg_mtllib ) );
                    continue;
                }

                data.push( CUnknown( line ) );
            }

        } catch( eof: haxe.io.Eof ) { }

        return data;
    }

    function parseVertex( reg : EReg ) {

        var x = Std.parseFloat( reg.matched(1) );
        var y = Std.parseFloat( reg.matched(2) );
        var z = Std.parseFloat( reg.matched(3) );
        var w = 1.0;
        if( reg.matched(4) != null )
            w = Std.parseFloat( reg.matched(4) );


        return CVertex({
            x: x,
            y: y,
            z: z,
            w: w,
        });
    }

    function parseVertexNormal( reg : EReg ) {

        var nx = Std.parseFloat( reg.matched(1) );
        var ny = Std.parseFloat( reg.matched(2) );
        var nz = Std.parseFloat( reg.matched(3) );

        return CNormal({
            i: nx,
            j: ny,
            k: nz,
        });
    }

    function parseVertexTexture( reg : EReg ) {

        var u = Std.parseFloat( reg.matched(1) );
        var v = 0.0;
        if( reg.matched(2) != null )
            v = Std.parseFloat( reg.matched(2) );
        var w = 0.0;
        if( reg.matched(3) != null )
            w = Std.parseFloat( reg.matched(3) );

        return CTextureUV({
            u: u,
            v: v,
            w: w,
        });
    }

    function parseGroupName( reg : EReg ) {

        var groupNames = reg.matched(1)
            .trim()
            .split(' ')
            .filter( function(gn) return gn.trim().length > 0 );

        return CGroup( groupNames );
    }

    function parseMaterialLibrary( reg : EReg ) {

        var libraries = reg.matched(1)
            .trim()
            .split(' ')
            .filter( function(gn) return gn.trim().length > 0 );

        return CMtlLib( libraries );
    }

    function parseObjectName( reg : EReg) {

        var objectName = reg.matched(1);
        if( objectName != null ) {
            objectName = objectName.trim();
        }

        return CObject( objectName );
    }

    function parseUseMaterial( reg : EReg) {

        var materialName = reg.matched(1).trim();

        return CMaterial( materialName );
    }

    function parseFace( reg : EReg ) {

        var points = reg.matched(1).split(' ').filter( function(p) return p.length > 0 );

        var face : Face = [];

        for( p in points ) {

            var t = p.split('/').map(function(s) return Std.parseInt(s));
            t.push(null);
            t.push(null);
            t.splice(3, 2);
            if( t[ 0 ] == null ) {
                return CUnknown( reg.matched( 0 ) );
            }

            var fv : FaceVertex = {
                vertexIndex: t[0],
                uvIndex: t[1],
                normalIndex: t[2],
            }

            face.push( fv );
        }

        return CFace( face );
    }

    function parseVertexParametric( reg : EReg ) {

        var u = Std.parseFloat( reg.matched(1) );
        var v = Std.parseFloat( reg.matched(2) );
        var w = 1.0;
        if( reg.matched(3) != null )
            w = Std.parseFloat( reg.matched(3) );


        return CParametric({
            u: u,
            v: v,
            w: w,
        });
    }
}